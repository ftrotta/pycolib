Welcome to ftrotta.pycolib documentation
========================================

A collection of tools of common usage in Python.

Please find the source code at https://gitlab.com/ftrotta/pycolib

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
