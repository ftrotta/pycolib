# pylint: disable=missing-docstring
import os


def detect_src_path() -> str:
    pwd = os.getcwd()
    i = pwd.find("tests")
    if i > -1:
        ret = pwd[:i]
    else:
        ret = pwd
    return ret
