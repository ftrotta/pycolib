
rm -r src/.pylint
rm -r src/.pytest_cache
rm -r src/ftrotta.pycolib.egg-info
rm -r src/.coverage*
rm -r docs/_build
rm -r .eggs/
rm -r build/
rm -r dist/
